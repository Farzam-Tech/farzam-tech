# Data-Science-Portfolio

#### Author: Farzam Ajili

#### E-Mail: Fajili@ryerson.ca


This repository contains data science and machine learning repositories.

---

# Data Analysis and Visualization 

* #### [Portfolio Analyzer](https://gitlab.com/Farzam-Tech/portfolio-analyzer)
An analysis notebook that analyzes and visualizes the major metrics of the portfolios to determine which portfolio is performing the best across multiple areas: volatility, returns, risk, and Sharpe ratios, and also determine which portfolio outperformed the others.

# SQL - Data Modeling , Data Engineering , Data Analysis

* #### [Looking for Suspicious Transactions](https://gitlab.com/Farzam-Tech/looking-for-suspicious-transactions)
Fraud is prevalent these days, whether you are a small taco shop or a large international business. While there are emerging technologies that employ machine learning and artificial intelligence to detect fraud, many instances of fraud detection still require strong data analytics to find abnormal charges.
In this project,I have applied SQL skills to analyze historical credit card transactions and consumption patterns in order to identify possible fraudulent transactions.

1. Data Modeling: Define a database model to store the credit card transactions data and create a new PostgreSQL database using our model.

2. Data Engineering: Create a database schema on PostgreSQL and populate our  database from the CSV files provided.

3. Data Analysis: Analyze the data to identify possible fraudulent transactions trends data, and develop a report of our observations.

# Machine Learning


## Time Series Analysis

* #### [BTC Price Forecast](https://gitlab.com/Farzam-Tech/btc-price-forcast)
In this project we will test the many time series tools in order to predict future movements in the value of Bitcoin versus the USA dollar and also
 we will build a Scikit-Learn linear regression model to predict BTC/USD returns with lagged BTC/USD futures returns and categorical calendar seasonal effects.


## Classification

* #### [Risky Business](https://gitlab.com/Farzam-Tech/risky-business)
build and evaluate several machine learning models to predict credit risk using data we'd typically see from peer-to-peer lending services. Credit risk is an inherently imbalanced classification problem (the number of good loans is much larger than the number of at-risk loans), so we will need to employ different techniques for training and evaluating models with imbalanced classes. we will use the imbalanced-learn and Scikit-learn libraries to build and evaluate models using the two following techniques:
Resampling
Ensemble Learning

* #### [Diabetes Prediction Project(Supervised Learning)](https://gitlab.com/Farzam-Tech/Diabetes-Prediction-Project)
In this project, I have built a classifier to predict Diabetes disease.
I have implemented different classification models on the dataset and evaluated the performance of the models.

## NLP (Natural Language Processing)

* #### [Bitcoin Ethereum News Sentiment Analysis](https://gitlab.com/Farzam-Tech/nlp)

In this project, I have applied natural language processing to understand the sentiment in the latest news articles featuring Bitcoin and Ethereum. I will also apply fundamental NLP techniques to better understand the other factors involved with the coin prices such as common words and phrases and organizations and entities mentioned in the articles.

## AWS Cloud

#### AWS Sagemaker

* #### [Clustering Crypto(Unsupervised Learning (k-means))](https://gitlab.com/Farzam-Tech/clustering-crypto)
In this project, I put Unsupervised-learning and Amazon SageMaker skills into action by clustering cryptocurrencies and creating plots to present my results by generating a report of what cryptocurrencies are available on the trading market and how they can be grouped using classification.


#### AWS Lex and AWS Lambda

* #### [Crypto Converter](https://gitlab.com/Farzam-Tech/crypto-converter)

In this project I have created a bot using AWS-LEX and AWS-Lambda to allow customers to convert from dollars to one of these three cryptocurrencies: Bitcoin, Ethereum, and Ripple.

## Deep Learning - neural networks

* #### [LSTM Stock Predictor](https://gitlab.com/Farzam-Tech/lstm-stock-predictor)

In this project I have used deep learning recurrent neural networks to model bitcoin closing prices. One model will use the FNG indicators to predict the closing price while the second model  uses a window of closing prices to predict the nth closing price.

## Algorithm Traiding 

* #### [High Frequency Trading Algorithm](https://gitlab.com/Farzam-Tech/high-frequency-trading-algorithm)
A trading algorithms to produce consistent investment performance capable of outperforming the markets based on an investing strategy which is the combination of Machine Learning Algorithms and High Frequency Algorithms. This strategy has become very popular and requires skills from multiple of the previous units, such as automatically retrieving data used to make investment decisions, training a model, and building an algorithm to execute the trading. The algorithm is based on stock market data for FB, AMZN, AAPL, NFLX, GOOGL, MSFT, and TSLA at the minute level. It should conduct buys and sells every minute based on 1 min, 5 min, and 10 min Momentum.

* #### [Machine Learning Trading Bot](https://gitlab.com/Farzam-Tech/machine-learning-trading-bot)

I have combined new algorithmic trading skills in financial Python programming and machine learning to create an algorithmic trading bot that learns and adapts to new data and evolving markets.
In a Jupyter notebook:
Implement an algorithmic trading strategy that uses machine learning to automate the trade decisions.
Adjust the input parameters to optimize the trading algorithm.
Train a new machine learning model and compare its performance to that of a baseline model.





